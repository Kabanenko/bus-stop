import React, {Component} from 'react';

import GoogleMap from 'google-map-react';
import BusStop from './BusStop';

class BusStopMap extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="google-map">
                <GoogleMap
                    key={this.props.center.lat+this.props.center.lng}
                    apiKey='AIzaSyAPLYilyXmO2Tt_sdmrcLdTHoq060pMlVA'
                    zoom={14}
                    center={this.props.center}>
                    { this.props.busStops.map((item) =>
                        <BusStop key={item.atcocode} lat={item.latitude} lng={item.longitude} stop={item}/>
                    )}
                </GoogleMap>
            </div>
        );
    }
}
export default BusStopMap;
