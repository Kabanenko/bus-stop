import React, {Component} from 'react';


class BusStop extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
                <div className="map-marker">
                    <span>{this.props.stop.name}</span>
                    <span> <b>SMS:</b>{this.props.stop.smscode}</span>
                </div>
        );
    }
}
export default BusStop;