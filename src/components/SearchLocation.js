import React, {Component} from 'react';
import _ from 'lodash';
import logo from '../images/bus.svg';

class SearchLocation extends Component {
    constructor(props) {
        super(props);
        this.state = {location: '', center: {lat: '', lng: ''}, isSearching: false};
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState(_.merge(this.state, {location: event.target.value}));
    }

    handleSubmit(event) {
        this.handleSearch();
        event.preventDefault();
    }


    handleSearch() {
        let self = this;
        self.setState(_.merge(self.state, {isSearching: true}));
        self.get('https://maps.google.com/maps/api/geocode/json?address=' + self.state.location + ',london&sensor=false&key=AIzaSyBuUOYTKxmhHKsxqenJUfe3guH6doyM0Ew').then(
            (response) => {
                let data = JSON.parse(response);
                return data.results.length > 0 ? data.results["0"].geometry.location : {
                        center: {
                            lat: '',
                            lng: ''
                        }
                    };
            }
        ).then((geometry) => {
            self.get('http://transportapi.com/v3/uk/bus/stops/near.json?lat=' + geometry.lat + '&lon=' + geometry.lng + '&api_key=6595ac315d0d3c31e2649cb82b5d885e&app_id=9f325c50').then(
                (response) => {
                    let data = JSON.parse(response);
                    self.setState(_.merge(self.state,
                        {isSearching: false},
                        {busStops: data['stops']},
                        {center: {lat: geometry.lat, lng: geometry.lng}}
                    ));
                    this.props.onUpdate(this.state);
                    return true;
                }
            )
        })
    }

    get(url) {
        return new Promise(function (resolve, reject) {

            let xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);

            xhr.onload = function () {
                if (this.status === 200) {
                    resolve(this.response);
                } else {
                    let error = new Error(this.statusText);
                    error.code = this.status;
                    reject(error);
                }
            };

            xhr.onerror = function () {
                reject(new Error("Something went wrong"));
            };

            xhr.send();
        });

    }

    render() {
        return (
            <div>
                <div className="search-location">
                    <form onSubmit={this.handleSubmit}>
                        <div className="flex-row">
                            <div className="col">
                                <img src={logo} className="logo" alt="Bus Icon"/>
                            </div>
                            <div className="col">
                                <label>
                                    Search bus stops:
                                </label>
                            </div>
                            <div className="col">
                                <input type="text" value={this.state.location} onChange={this.handleChange}
                                       className="form-element" placeholder="Ex: waterloo"/>
                            </div>
                            <div className="col">
                                <input type="submit" value="Submit" className="btn btn-primary"/>
                            </div>
                            <div className="col">
                                <div className={this.state.isSearching ? 'loader' : 'loader hidden'}></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default SearchLocation;
