import React, {Component} from 'react';
import BusStopMap from './components/BusStopMap';
import SearchLocation from './components/SearchLocation';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {busStops: [], center: {lat: 51.5074, lng: 0.1278}};
    }

    onUpdate(data) {
        this.setState({busStops: data.busStops, center : data.center});
    }

    render() {
        return (
            <div className="container">
                <SearchLocation onUpdate={this.onUpdate.bind(this)}/>
                <BusStopMap center={this.state.center} busStops={this.state.busStops}/>
            </div>
        );
    }
}

export default App;
